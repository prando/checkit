/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.playground;


import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.*;
import com.checkit.persistence.framework.Accounts;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author Paul
 */
public class ModulePlayground {
    
    private static Database db = null;
    private static PersistenceUtilities utils = null;
    
    public ModulePlayground() {
        db = Database.getInstance();
        utils = new PersistenceUtilities();
    }
    
    /*
    public void displayAccountInformation() {
        ArrayList<AccountAbs> accounts = Accounts.getAllAccounts();
        for(AccountAbs account : accounts) {
            System.out.println(account);
            displayTransactionInformation(account);
            System.out.println();
        }
        
        
    }
    
    public void displayTransactionInformation(AccountAbs account) {
        ArrayList<Transaction> transactions = account.getAllTransactions();
        for(Transaction transaction : transactions) {
            System.out.println("   ---> Number: " + transaction.getNumber() + ", Amount: " + transaction.getAmount() + ", Date: " + transaction.getDate());
        }
    }
    */
    
    public Checking addCheckingAccountWithTransactions() throws PersistenceException {
        Checking checking = addCheckingAccount();
        
        for(int i = 0; i < 500; i++) {
            Transaction trx = new Transaction();
            switch(trx.getId()%3){
                case 0:
                    trx.setTrxType("w");
                    break;
                default:
                    trx.setTrxType("d");
                    break;                    
            }
            trx.setAmount(100.50 + i);            
            trx.setDate(Calendar.getInstance());
            checking.insertTransaction(trx);
        }

        return checking;
    }
    
    public Checking addCheckingAccount() throws PersistenceException {
        Checking checking = new Checking("Account", 500, Calendar.getInstance());
        
        Accounts.insertAccount(checking);
        
        System.out.println("InsertAccountTransaction returns id: " + checking.getId());
        return checking;
    }
    
    public void coldStartDatabase() throws Exception {
        //utils.deleteFileByName("checkItMaster.db");
        //utils.deleteFileByName("sequences_checkItMaster.db");
        db.coldstart();
    }
    
    public static void main(String[] args) throws Exception {
        ModulePlayground mod = new ModulePlayground();
        //System.out.println(utils.isFileNameExists("checkItMaster.db"));
        File dbFile = new File("checkItMaster.db");
        
        db.loadDatabase(dbFile);
        
        //mod.sampleOpenDatabaseSaveAccounts1(db);
        //mod.addCheckingAccount();
        mod.addCheckingAccountWithTransactions();
        
        //mod.displayAccountInformation();
        
        db.closeDatabase();
    }
    
}
