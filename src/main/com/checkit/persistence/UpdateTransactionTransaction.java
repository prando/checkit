package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;

/**
 *
 * @author Paul
 * 
 * This class handles Transaction insert into database
 */
public class UpdateTransactionTransaction extends UpdateTransaction {

    public UpdateTransactionTransaction(Transaction transaction, AccountAbs account) throws PersistenceException {
        super(transaction, account);
    }
    
    @Override
    protected void update() throws PersistenceException {
        TransactionProxy transactionProxy = (TransactionProxy)proxy;
        db.update(transactionProxy);

    }
    
}
