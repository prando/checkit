package com.checkit.persistence;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author prando
 * 
 * This sequences class is similar to sequence handling in databases to achieve returning a unique identifier (getNextSequence()).
 * This class implements PersistenceObjectIfc which is serializable.
 * 
 */
public class Sequences implements PersistenceObjectIfc {
    
    private Map<String, Integer> sequences = null;
    
    
    public Sequences() {
        sequences = new HashMap<String, Integer>();
        reset();
    }
    
    /**
     * Resets the next sequence map to 0
     */
    public void reset() {
        sequences.put("next", 0);
    }
    
    /**
     * Returns next sequence integer from sequences map.  This will update internal map to returned sequence id.
     * @return 
     */
    public int getNextSequence() {
        int nextSequence = sequences.get("next") + 1;
        sequences.put("next", nextSequence);

        //System.out.println("Getting NextSequence, Id:" + nextSequence);
        return nextSequence;
    }
    
    /**
     * Returns the next sequence from sequences map.  This will NOT update internal map to returned sequence id.
     * @return 
     */
    public int getPeekNextSequence() {
        return sequences.get("next") + 1;
    }

    @Override
    public int checkSum() {
        return hashCode();
    }
    
}
