package com.checkit.persistence;

/**
 *
 * @author prando
 * 
 * Exception thrown when sequences not found.
 */
public class SequencesNotFoundException extends PersistenceException {
    
    public SequencesNotFoundException(String msg) {
        super("Sequences Exception: " + msg);
    }
}
