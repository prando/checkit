/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence.framework;

import com.checkit.persistence.Proxy;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Paul
 */
public class AccountProxy extends Proxy {
     
    public AccountProxy() {

    }
    
    
    @Override
    public int compareTo(Object obj) {
        if(obj instanceof AccountProxy) {
            AccountProxy acct = (AccountProxy)obj;
            if(acct.getVersionCnt() > getVersionCnt()) {
                return 1;
            }
            else if(acct.getVersionCnt() == getVersionCnt()) {
                return 0;
            }
            else {
                return -1;
            }
        }
        return -1;
    }
    

    
    @Override
    public int hashCode() {
        return getId();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof AccountProxy) {
            AccountProxy proxy = (AccountProxy)obj;
            if(proxy.getId() == getId()) {
                return true;
            }
        }
        return false;
    }
    
    public String toString() {
        return "AccountProxy";
    }

    @Override
    public int checkSum() {
        return getId();
    }
    
}
