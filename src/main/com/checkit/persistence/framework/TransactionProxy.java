/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence.framework;

import com.checkit.persistence.Proxy;
import java.util.Date;

/**
 *
 * @author Paul
 */
public class TransactionProxy extends Proxy {
    
    private int accountId = -1;
    
    public TransactionProxy() {
    
    }

    public void setAccountId(int id) {
        this.accountId = id;
    }
    
    public int getAccountId() {
        return this.accountId;
    }

    @Override
    public int checkSum() {
        return getId();
    }
    
    @Override
    public int compareTo(Object obj) {
        if(obj instanceof TransactionProxy) {
            TransactionProxy trx = (TransactionProxy)obj;
            if(trx.getVersionCnt() > getVersionCnt()) {
                return 1;
            }
            else if(trx.getVersionCnt() == getVersionCnt()) {
                return 0;
            }
            else {
                return -1;
            }
        }
        return -1;
    }
    
    @Override
    public int hashCode() {
        return getId();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof TransactionProxy) {
            TransactionProxy proxy = (TransactionProxy)obj;
            if(proxy.getId() == getId()) {
                return true;
            }
        }
        return false;
    }
    
    public String toString() {
        return "TransactionProxy";
    }
    
}
