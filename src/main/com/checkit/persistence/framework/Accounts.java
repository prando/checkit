/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence.framework;

import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Savings;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.Database;
import com.checkit.persistence.DeleteAccountTransaction;
import com.checkit.persistence.DeleteTransactionTransaction;
import com.checkit.persistence.InsertAccountTransaction;
import com.checkit.persistence.InsertBulkTransactionTransaction;
import com.checkit.persistence.InsertTransactionTransaction;
import com.checkit.persistence.PersistenceException;
import com.checkit.persistence.ProxyAttributeNameException;
import com.checkit.persistence.UpdateAccountTransaction;
import com.checkit.persistence.UpdateTransactionTransaction;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Paul
 */
public class Accounts {
    
    
    /**
     * Retrieves all Transactions for this Account (by Id) from the database
     * @param accountId
     * @return 
     */
    public static ArrayList<Transaction> getAllTransactionsByAccountId(int accountId) {
        ArrayList<Transaction> list = new ArrayList<Transaction>();
        Database db = Database.getInstance();
        try {
            ArrayList<TransactionProxy> trxProxies = db.getAllActiveTransactionProxiesByAccountId(accountId);
            Iterator<TransactionProxy> iTrxProxies = trxProxies.iterator();
            while(iTrxProxies.hasNext()) {
                TransactionProxy trxProxy = iTrxProxies.next();
                Transaction trx = new Transaction();
                try {
                    trx.mergePersistenceProxy(trxProxy);
                    list.add(trx);
                }
                catch(ProxyAttributeNameException e) {
                    e.printStackTrace();
                }

            }
        }
        catch(PersistenceException e) {
            e.printStackTrace();
        }

        return list;
    }
    
    public static Transaction getTransactionById(int acctId, int id) {
        ArrayList<Transaction> list = getAllTransactionsByAccountId(acctId);
        for (Transaction trx : list) {
            if (trx.getId() == id) {
                return trx;
            }
        }
        return null;
    }

    public static ArrayList<AccountAbs> getAllAccounts() {
        
        
        ArrayList<AccountAbs> list = new ArrayList<AccountAbs>();
        Database db = Database.getInstance();

        try {
            ArrayList<AccountProxy> proxies = db.getAllActiveAccountProxies();
            for (AccountProxy proxy : proxies) {
                AccountAbs acct = null;
                if (proxy instanceof CheckingAccountProxy) {
                    acct = new Checking((CheckingAccountProxy)proxy);
                }
                else if (proxy instanceof SavingsAccountProxy) {
                    acct = new Savings((SavingsAccountProxy)proxy);
                }
                list.add(acct);
                    
            }
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static AccountAbs getAccountById(int id) {
        ArrayList<AccountAbs> list = getAllAccounts();
        for (AccountAbs account : list) {
            if (account.getId() == id) {
                return account;
            }
        }
        return null;
    }

    /*
    public static ArrayList<AccountAbs> getAccountsByName(String name) {
        ArrayList<AccountAbs> refined = new ArrayList<AccountAbs>();
        ArrayList<AccountAbs> list = getAllAccounts();
        for (AccountAbs account : list) {
            if (account.getName().equalsIgnoreCase(name)) {
                refined.add(account);
            }
        }
        return refined;
    }
    */

    public static ArrayList<AccountAbs> getAccountsByNumber(int number) {
        ArrayList<AccountAbs> refined = new ArrayList<AccountAbs>();
        ArrayList<AccountAbs> list = getAllAccounts();
        for (AccountAbs account : list) {
            if (account.getAccountNumber() == number) {
                refined.add(account);
            }
        }
        return refined;
    }
    
    
    public static void insertTransaction(Transaction trx, AccountAbs account) throws PersistenceException {
        if(account.getId() <= 0) {
            throw new PersistenceException("Account must first be saved before inserting transaction.");
        }
        new InsertTransactionTransaction(trx, account);
    }
    
    public static void deleteTransaction(Transaction trx, AccountAbs account) throws PersistenceException {
        if(account.getId() <= 0) {
            throw new PersistenceException("Account must first be saved before deleting transaction.");
        }
        new DeleteTransactionTransaction(trx, account);
    }
    
    public static void updateTransaction(Transaction trx, AccountAbs account) throws PersistenceException {
        if(account.getId() <= 0) {
            throw new PersistenceException("Account must first be saved before updating transaction.");
        }
        new UpdateTransactionTransaction(trx, account);
    }
    
    public static void insertBulkTransactions(ArrayList<Transaction> trxs, AccountAbs account) throws PersistenceException {
        if(account.getId() <= 0) {
            throw new PersistenceException("Account must first be saved before inserting transaction.");
        }
        new InsertBulkTransactionTransaction(trxs, account);
    }    
    
    public static void insertAccount(AccountAbs account) throws PersistenceException {
        new InsertAccountTransaction(account);
    }
    
    public static void deleteAccount(AccountAbs account) throws PersistenceException {
        new DeleteAccountTransaction(account);
    }
    
    public static void updateAccount(AccountAbs account) throws PersistenceException {
        new UpdateAccountTransaction(account);
    }
    
}
