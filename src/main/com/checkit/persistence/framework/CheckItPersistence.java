/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence.framework;

import com.checkit.persistence.Proxy;
import com.checkit.persistence.ProxyAttributeNameException;

/**
 *
 * @author Paul
 */
public interface CheckItPersistence {
    
    public abstract void mergePersistenceProxy(Proxy proxy) throws ProxyAttributeNameException;
    
    public abstract Proxy getPersistenceProxy();
    
    public int getId();
}
