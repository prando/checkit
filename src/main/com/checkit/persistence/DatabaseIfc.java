package com.checkit.persistence;

import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;
import java.io.File;
import java.util.ArrayList;
import java.util.Set;

/**
 *
 * @author Paul
 * Database interface class
 */
public interface DatabaseIfc {
    
    public void loadDatabase(File file) throws PersistenceException;
    
    public int insert(Proxy proxy) throws PersistenceException;
    public void delete(Proxy proxy) throws PersistenceException;
    public void update(Proxy proxy) throws PersistenceException;
    public Proxy selectById(int id);
    
    public void commit() throws PersistenceException;
    public void coldstart() throws PersistenceException;
    public void deleteAll() throws PersistenceException;

    public ArrayList<AccountProxy> getAllActiveAccountProxies() throws PersistenceException;
    public ArrayList<TransactionProxy> getAllActiveTransactionProxiesByAccountId(int accountId) throws PersistenceException;

    
    
}
