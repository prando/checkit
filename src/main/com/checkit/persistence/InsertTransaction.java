package com.checkit.persistence;

import com.checkit.persistence.framework.CheckItPersistence;


/**
 *
 * @author Paul
 * 
 * This abstract class is a persistence transaction class which must be extended to handle inserts to persistence layer.
 * This class accepts a persistence business object.  During processing, the proxy class will be extracted and processed against the database.
 */
public abstract class InsertTransaction extends PersistenceTransaction {
    
    protected abstract int insert() throws PersistenceException;
    
    public InsertTransaction(CheckItPersistence obj) throws PersistenceException {
        super(obj, null);
        processPersistence();
    }
    
    public InsertTransaction(CheckItPersistence obj, CheckItPersistence obj2) throws PersistenceException {
        super(obj, obj2);
        processPersistence();
    }
    
    protected void process() throws PersistenceException {

        int id = insert();
        System.out.println("New Id Inserted: " + id);
        proxy.setId(id);
        //businessObj.setId(id);
    }
}
