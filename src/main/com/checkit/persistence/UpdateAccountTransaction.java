package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.persistence.framework.AccountProxy;


/**
 *
 * @author Paul
 */
public class UpdateAccountTransaction extends UpdateTransaction {

    public UpdateAccountTransaction(AccountAbs account) throws PersistenceException {
        super(account);
    }
    
    @Override
    protected void update() throws PersistenceException {
        AccountProxy accountProxy = (AccountProxy)proxy;
        db.update(accountProxy);
    }
    
}
