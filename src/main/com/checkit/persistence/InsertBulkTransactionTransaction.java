package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.Accounts;
import com.checkit.persistence.framework.CheckItPersistence;
import com.checkit.persistence.framework.TransactionProxy;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Paul
 * This class handles bulk transaction insert of Transactions.  It will get all Account transactions and insert into the database.
 */
public class InsertBulkTransactionTransaction extends BulkInsertTransaction {


    public InsertBulkTransactionTransaction(ArrayList<Transaction> trxs, AccountAbs account) throws PersistenceException {
        super(trxs, account);
    }
    
    @Override
    protected Integer[] insert() throws PersistenceException {
        ArrayList<Integer> ids = new ArrayList<Integer>();
        
        AccountAbs account = (AccountAbs)businessObj;
        Iterator<Transaction> iTrx = (Iterator<Transaction>)iterateBulk();
        while(iTrx.hasNext()) {
            Transaction trx = iTrx.next();
            TransactionProxy trxProxy = (TransactionProxy)trx.getPersistenceProxy();
            trxProxy.setAccountId(proxy.getId());
            int id = db.insert(trxProxy);
            ids.add(id);
        }

        return ids.toArray(new Integer[0]);
    }
    
}
