package com.checkit.persistence;

import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;

/**
 *
 * @author Paul
 * 
 * Main class which handles serialization persistence to single file.
 */
public class Database implements DatabaseIfc {

    private static Database instance = null;
    private Sequences sequences = null;

    private boolean dbLoaded = false;
    private String origDbFilename = null;
    
    private File activeDbFile = null;
    private File origDbFile = null;
    private File lockDbFile = null;
    
    private PersistenceUtilities utils = null;

    private ArrayList<Proxy> proxies = null;
    

    /*
     * Private Constructor for Database.  Singleton.  getInstance() method must be used to load Database class.
     */
    private Database() {
        utils = new PersistenceUtilities();
        proxies = new ArrayList<Proxy>();
    }

    
    /*
     * 
     * loadDatabase(File) must be called before any persistence actions may take place.
     */
    @Override
    public synchronized void loadDatabase(File file) throws PersistenceException {
        if (dbLoaded) {
            throw new PersistenceException("Database has already been loaded!");
        }

        try {
            origDbFile = file;

            if (!utils.isFileExists(origDbFile)) {
                utils.createFile(origDbFile.getAbsolutePath());
            }

            origDbFilename = origDbFile.getName();
            String tempDbFilename = "locked_" + origDbFilename;

            if (utils.isFileNameExists(tempDbFilename)) {
                utils.deleteFileByName(tempDbFilename);
            }



            lockDbFile = utils.copyFile(origDbFile, new File(tempDbFilename));
            setActiveDbFile(origDbFile);

            dbLoaded = true;

            //Load Sequences First
            try {
                sequences = getSequencesFromDatabase();
            } catch (SequencesNotFoundException e) {
                //e.printStackTrace();
                sequences = createNewSequences();
                commit();
            }


            refreshCache();



        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void printAllProxies() {
        debugProxies();
    }
    
    public String getOrigDbFilename() {
        return origDbFilename;
    }

    public synchronized void closeDatabase() throws PersistenceException {
        debug("Entering closeDatabase()");
        utils.deleteFile(lockDbFile);
        dbLoaded = false;
    }
    
    
    
    public synchronized void switchDatabase(File file) throws PersistenceException {
        closeDatabase();
        loadDatabase(file);
    }

    private void refreshCache() throws PersistenceException {
        debug("Entering refreshCache()");
        proxies = getAllActiveProxies();

    }
    
    public int getPeekNextSequence() {
        return sequences.getPeekNextSequence();
    }

    @Override
    public int insert(Proxy proxy) throws PersistenceException {
        debug("Entering insert(proxy) " + proxy);
        
        int nextId = sequences.getNextSequence();
        //System.out.println("nextId: " + nextId);
        proxy.setId(nextId);
        
        if(proxies.contains(proxy)) {
            debugProxies();
            throw new PersistenceException("Unable to Insert!  Proxy already exists!  Id: " + proxy.getId());
        }
        proxies.add(proxy);
        
        return nextId;
    }
    
    @Override
    public void delete(Proxy proxy) throws PersistenceException {
        debug("Entering delete(proxy)" + proxy);
        if(!proxies.contains(proxy)) {
            debugProxies();
            throw new PersistenceException("Unable to Delete! Proxy does not exist! Id: " + proxy.getId());
        }
        
        proxies.remove(proxy);
    }
    
    @Override
    public void update(Proxy proxy) throws PersistenceException {
        debug("Entering update(proxy)" + proxy);
        if(!proxies.contains(proxy)) {
            debugProxies();
            throw new PersistenceException("Unable to Update! Proxy does not exist! Id: " + proxy.getId());
        }
        if(proxy.getId() == -1) {
            throw new PersistenceException("Unable to Update! Proxy Id does not exist!");
        }
        
        int index = proxies.indexOf(proxy);
        proxies.set(index, proxy);
        
    }
    
    @Override
    public Proxy selectById(int id) {
        Iterator<Proxy> iProxies = proxies.iterator();
        while(iProxies.hasNext()) {
            Proxy proxy = iProxies.next();
            if(proxy.getId() == id) {
                return proxy;
            }
        }
        
        return null;
    }
    
    @Override
    public synchronized void commit() throws PersistenceException {
        debug("Entering commit()");
        getInstance();
        checkDatabaseLoaded();
        
        ArrayList<PersistenceObjectIfc> pObjsToCommit = new ArrayList<PersistenceObjectIfc>();
        
        //Add Sequence First
        pObjsToCommit.add(sequences);
        
        //Add Proxies Second
        Iterator<Proxy> iProxies = proxies.iterator();
        while(iProxies.hasNext()) {
            Proxy proxy = iProxies.next();
            //proxy.debug();
            pObjsToCommit.add(proxy);
        }
        
        //Commit to database
        debug("Commit Size: " + pObjsToCommit.size());
        Iterator<PersistenceObjectIfc> iToCommit = pObjsToCommit.iterator();
        while(iToCommit.hasNext()) {
            PersistenceObjectIfc pObj = iToCommit.next();
            if(pObj instanceof Proxy) {
                Proxy proxyObj = (Proxy)pObj;
                debug("Proxy to commit: " + pObj.getClass() + ", Id: " + proxyObj.getId());
            }
            
        }
        
        writePersistenceObjectsToDatabase(pObjsToCommit);
        
    }
 

    public ArrayList<AccountProxy> getAllActiveAccountProxies() throws PersistenceException {
        debug("Entering getAllActiveAccountProxies()");
        ArrayList<AccountProxy> aProxies = new ArrayList<AccountProxy>();
        Iterator<Proxy> iProxies = proxies.iterator();
        while(iProxies.hasNext()) {
            Proxy proxy = iProxies.next();
            if(proxy instanceof AccountProxy) {
                aProxies.add((AccountProxy)proxy);
            }
        }
        
        return aProxies;
    }
    
    public ArrayList<TransactionProxy> getAllActiveTransactionProxiesByAccountId(int accountId) throws PersistenceException {
        debug("Entering getAllActiveTransactionProxiesByAccountId()");
        ArrayList<TransactionProxy> tProxies = new ArrayList<TransactionProxy>();
        Iterator<Proxy> iProxies = proxies.iterator();
        while(iProxies.hasNext()) {
            Proxy proxy = iProxies.next();
            if(proxy instanceof TransactionProxy) {
                TransactionProxy tProxy = (TransactionProxy)proxy;
                if(tProxy.getAccountId() == accountId) {
                    tProxies.add(tProxy);
                }
            }
        }
        
        return tProxies;
    }


    private ArrayList<Proxy> getAllActiveProxies() {
        debug("Entering getActiveProxyItems()");
        ArrayList<Proxy> proxies = new ArrayList<Proxy>();
        try {
            ArrayList<PersistenceObjectIfc> objs = getPersistenceObjectsFromDatabase();
            for (PersistenceObjectIfc obj : objs) {
                if (obj instanceof Proxy) {
                    Proxy proxy = (Proxy) obj;
                    if (!proxy.isDeleted()) {
                        proxies.add((Proxy) obj);
                    }

                }
            }

        } catch (PersistenceException e) {
            e.printStackTrace();
        }

        return proxies;
    }

    private Sequences getSequencesFromDatabase() throws SequencesNotFoundException {
        try {
            Iterator<PersistenceObjectIfc> iPObj = getPersistenceObjectsFromDatabase().iterator();
            while (iPObj.hasNext()) {
                PersistenceObjectIfc pObj = iPObj.next();
                if (pObj instanceof Sequences) {
                    Sequences seqs = (Sequences) pObj;
                    return seqs;
                }
            }

        } catch (PersistenceException e) {
            e.printStackTrace();
        }

        throw new SequencesNotFoundException("Sequences Not Found!");
    }

    private Sequences createNewSequences() {
        return new Sequences();
    }
    
    private void writePersistenceObjectsToDatabase(ArrayList<PersistenceObjectIfc> pObjs) throws PersistenceException  {
        try {
            utils.writeObjectsToFile(getActiveDbFile(), pObjs);
        }
        catch(IOException e) {
            e.printStackTrace();
            throw new PersistenceException("Unable to write persistence objects to database.");
        }
        
    }

    private ArrayList<PersistenceObjectIfc> getPersistenceObjectsFromDatabase() throws PersistenceException {
        try {
            ArrayList<PersistenceObjectIfc> pObjects = new ArrayList<PersistenceObjectIfc>();
            Iterator<Object> iObj = getObjectsFromDatabase().iterator();
            while (iObj.hasNext()) {
                Object obj = iObj.next();
                if (obj instanceof PersistenceObjectIfc) {
                    pObjects.add((PersistenceObjectIfc) obj);
                }
            }
            return pObjects;
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        throw new PersistenceException("Unable to load persistence objects");
    }

    private ArrayList<Object> getObjectsFromDatabase() throws PersistenceException, IOException, ClassNotFoundException {
        getInstance();
        checkDatabaseLoaded();
        return utils.readObjectsFromFile(getActiveDbFile());
    }

    private void setActiveDbFile(File file) {
        activeDbFile = file;
    }

    private File getActiveDbFile() {
        return activeDbFile;
    }

    public boolean isDbLoaded() {
        return dbLoaded;
    }

    private void checkDatabaseLoaded() throws PersistenceException {
        if (!isDbLoaded()) {
            throw new PersistenceException("Database Not Loaded!");
        }
    }

    private void debug(String msg) {
        if(PersistenceConstants.DEBUG) {
            System.out.println(msg);
        }
    }
    
    private void debugProxies() {
        System.out.println("Debug Current Proxies...");
        Iterator<Proxy> iProxies = proxies.iterator();
        while(iProxies.hasNext()) {
            Proxy proxy = iProxies.next();
            System.out.println("Id: " + proxy.getId() + ", " + proxy.getClass());
        }
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    @Override
    public synchronized void coldstart() throws PersistenceException {
        debug("Entering coldstart()");
        getInstance();
        checkDatabaseLoaded();
        
        ArrayList<PersistenceObjectIfc> pObjsToCommit = new ArrayList<PersistenceObjectIfc>();
        
        sequences.reset();
        //Add Sequence First
        pObjsToCommit.add(sequences);
        
        
        //Commit to database
        debug("Commit Size: " + pObjsToCommit.size());
        Iterator<PersistenceObjectIfc> iToCommit = pObjsToCommit.iterator();
        while(iToCommit.hasNext()) {
            PersistenceObjectIfc pObj = iToCommit.next();
            debug(pObj.toString());
        }
        
        writePersistenceObjectsToDatabase(pObjsToCommit);
    }
    
    @Override
    public synchronized void deleteAll() throws PersistenceException {
        debug("Entering deleteAll()");
        getInstance();
        checkDatabaseLoaded();
        
        ArrayList<PersistenceObjectIfc> pObjsToCommit = new ArrayList<PersistenceObjectIfc>();
        
        //Add Sequence First
        pObjsToCommit.add(sequences);
        
        
        //Commit to database
        debug("Commit Size: " + pObjsToCommit.size());
        Iterator<PersistenceObjectIfc> iToCommit = pObjsToCommit.iterator();
        while(iToCommit.hasNext()) {
            PersistenceObjectIfc pObj = iToCommit.next();
            debug(pObj.toString());
        }
        
        writePersistenceObjectsToDatabase(pObjsToCommit);
    }
}
