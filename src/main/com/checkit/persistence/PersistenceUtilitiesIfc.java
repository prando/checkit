package com.checkit.persistence;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Paul
 * Interface for PersistenceUtilities class
 * 
 */
public interface PersistenceUtilitiesIfc {
    
    public boolean renameFile(File file, String newName);
    
    public boolean isFileNameExists(String name);
    
    public boolean isFileExists(File file);
    
    public File createFile(String name) throws IOException;
    
    public File getFile(String name);
    
    public boolean deleteFileByName(String name);
    
    public boolean deleteFile(File file);
    
    public long getCheckSum(File file) throws IOException;
    
    public boolean checkReadPermissions(File file) throws IOException;
    
    public boolean checkWritePermissions(File file) throws IOException;
    
    public boolean checkSerialization(File file) throws IOException;
    
    public File copyFile(File src, File des) throws IOException;
    
    public void writeObjectToFile(File file, Object obj) throws IOException;
    
    public void writeObjectsToFile(File file, ArrayList<? extends Object> objs) throws IOException;
    
    public ArrayList<Object> readObjectsFromFile(File file) throws IOException, ClassNotFoundException;
    
}
