package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.persistence.framework.AccountProxy;


/**
 *
 * @author Paul
 * 
 * Performs delete transaction for Account classes.
 */
public class DeleteAccountTransaction extends DeleteTransaction {

    public DeleteAccountTransaction(AccountAbs account) throws PersistenceException {
        super(account);
    }
    
    @Override
    protected void delete() throws PersistenceException {
        AccountProxy accountProxy = (AccountProxy)proxy;
        db.delete(accountProxy);
    }
    
}
