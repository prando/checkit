package com.checkit.persistence;

import com.checkit.utils.TestSerializationObject;
import com.checkit.persistence.framework.AccountProxy;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Paul
 */
public class PersistenceUtilities implements PersistenceUtilitiesIfc {
    
    private boolean debug = false;

    @Override
    public long getCheckSum(File file) throws IOException {
        return file.length() + file.lastModified();
    }

    @Override
    public boolean checkReadPermissions(File file) throws IOException {
        return file.canRead();
    }

    @Override
    public boolean checkWritePermissions(File file) throws IOException {
        return file.canWrite();
    }

    @Override
    public boolean checkSerialization(File file) throws IOException {
        long timestamp = System.currentTimeMillis();

        TestSerializationObject testObj = new TestSerializationObject();
        testObj.id = 150;
        testObj.name = "testObj";
        testObj.timestamp = timestamp;

        writeObjectToFile(file, testObj);

        try {
            ArrayList<Object> objs = readObjectsFromFile(file);
            for (Object obj : objs) {
                if (obj instanceof TestSerializationObject) {
                    TestSerializationObject tObj = (TestSerializationObject) obj;
                    if (tObj.timestamp == timestamp) {
                        return true;
                    }
                }
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return false;

    }

    @Override
    public File copyFile(File src, File des) throws IOException {
        FileInputStream fIn = null;
        FileOutputStream fOut = null;
        
        fIn = new FileInputStream(src);
        fOut = new FileOutputStream(des);
        
        byte[] buffer = new byte[1024];
        int length = -1;
        while((length = fIn.read(buffer)) > 0) {
            fOut.write(buffer, 0, length);
        }
        
        fIn.close();
        fOut.close();
        
        return des;
    }

    /*
     * This will overwrite the file contents
     */
    @Override
    public void writeObjectToFile(File file, Object obj) throws IOException {
        FileOutputStream fOut = new FileOutputStream(file);
        ObjectOutputStream oOut = new ObjectOutputStream(fOut);
        oOut.writeObject(obj);
        oOut.close();


    }
    
    @Override
    public void writeObjectsToFile(File file, ArrayList<? extends Object> objs) throws IOException {
        FileOutputStream fOut = new FileOutputStream(file);
        ObjectOutputStream oOut = new ObjectOutputStream(fOut);
        for(Object obj : objs) {
            debug("Writing Object: " + obj);
            
            if(obj instanceof AccountProxy) {

            }
            
            oOut.writeObject(obj);
        }
        oOut.close();

    }

    @Override
    public ArrayList<Object> readObjectsFromFile(File file) throws IOException, ClassNotFoundException {
        ArrayList<Object> list = new ArrayList<Object>();

        FileInputStream fIn = null;
        ObjectInputStream oIn = null;

        try {
            fIn = new FileInputStream(file);
            oIn = new ObjectInputStream(fIn);
            Object obj = null;
            while ((obj = oIn.readObject()) != null) {
                debug("Reading Object: " + obj);
                
                if(obj instanceof AccountProxy) {

                }
                list.add(obj);
            }
        } catch (EOFException e) {//Do Nothing, EOF reached
        } finally {
            try {
                oIn.close();
            }
            catch(Exception e) {}
            
        }

        return list;
    }

    @Override
    public File createFile(String name) throws IOException {
        File file = getFile(name);
        file.createNewFile();
        return file;
    }

    @Override
    public boolean isFileNameExists(String name) {
        File file = getFile(name);
        if(file.exists()) {
            return true;
        }
        return false;
    }
    
    @Override
    public boolean isFileExists(File file) {
        if(file.exists()) {
            return true;
        }
        return false;
    }

    @Override
    public File getFile(String name) {
        File file = new File(name);
        return file;
    }

    @Override
    public boolean deleteFileByName(String name) {
        File file = getFile(name);
        return file.delete();
    }
    
    @Override
    public boolean deleteFile(File file) {
        return file.delete();
    }
    
    @Override
    public boolean renameFile(File file, String newName) {
        return file.renameTo(new File(newName));
    }
    
    
    private void debug(String msg) {
        if(debug) {
            System.out.println(msg);
        }
    }
}
