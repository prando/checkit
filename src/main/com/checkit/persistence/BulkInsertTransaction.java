package com.checkit.persistence;

import com.checkit.persistence.framework.CheckItPersistence;
import java.util.ArrayList;
import java.util.Iterator;


/**
 *
 * @author Paul
 * 
 * This abstract class may be extended to perform bulk insert transactions.
 */
public abstract class BulkInsertTransaction extends PersistenceTransaction {
    
    private ArrayList<? extends CheckItPersistence> insertBulk = null;
    
    protected abstract Integer[] insert() throws PersistenceException;
    
    public BulkInsertTransaction(ArrayList<? extends CheckItPersistence> insertBulk, CheckItPersistence obj) throws PersistenceException {
        super(obj, null);
        this.insertBulk = insertBulk;
        processPersistence();
    }
    
    public BulkInsertTransaction(ArrayList<? extends CheckItPersistence> insertBulk, CheckItPersistence obj, CheckItPersistence obj2) throws PersistenceException {
        super(obj, obj2);
        this.insertBulk = insertBulk;
        processPersistence();
    }
    
    protected void process() throws PersistenceException {

        Integer[] ids = insert();
        //proxy.setId(id);
        //businessObj.setId(id);
    }
    
    protected Iterator<? extends CheckItPersistence> iterateBulk() {
        return (Iterator<? extends CheckItPersistence>)insertBulk.iterator();
    }
    
}
