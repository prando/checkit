package com.checkit.persistence;

import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Paul
 * 
 * Base abstract class which is used for serialization.  This class is a proxy for business objects to persist persistence data only, without having to 
 * persist business logic as well.
 * 
 */
public abstract class Proxy implements Comparable, PersistenceObjectIfc {
    
    public abstract int checkSum();
    public Map<String, Object> persistObjs = null;
    private int id = -1;
    private long versionCnt = -1L;
    private boolean deleted = false;
    private long lastUpdated = -1;
    
    public Proxy() {
        versionCnt = 0;
        persistObjs = new HashMap<String, Object>();
    }
    
    /**
     * get String by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public String getString(String name) throws ProxyAttributeNameException {
        return (String)getObject(name);
    }
    
    /**
     * get double by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public double getDouble(String name) throws ProxyAttributeNameException {
        if(!persistObjs.containsKey(name)) {
            return -1D;
        }
        return (Double)getObject(name);
    }
    
    /**
     * get float by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public float getFloat(String name) throws ProxyAttributeNameException {
        if(!persistObjs.containsKey(name)) {
            return -1F;
        }
        return (Float)getObject(name);
    }    
    
    /**
     * get integer by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public int getInteger(String name) throws ProxyAttributeNameException {
        if(!persistObjs.containsKey(name)) {
            return -1;
        }
        return (Integer)getObject(name);
    }
    
    /**
     * get Date by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public Date getDate(String name) throws ProxyAttributeNameException {
        return (Date)getObject(name);
    }
    
    
    /**
     * get Calendar by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public Calendar getCalendar(String name) throws ProxyAttributeNameException {
        return (Calendar)getObject(name);
    }
    
    /**
     * get Object by proxy attribute name
     * @param name
     * @return
     * @throws ProxyAttributeNameException 
     */
    public Object getObject(String name) throws ProxyAttributeNameException {
        if(!persistObjs.containsKey(name)) {
            throw new ProxyAttributeNameException("Attribute name " + name + " does not exist!");
        }
        return persistObjs.get(name);
    }
    
    /**
     * set proxy object attribute
     * @param name
     * @param obj 
     */
    public void setObject(String name, Object obj) {
        persistObjs.put(name, obj);
    }
    
    /**
     * sets delete flag on proxy
     */
    public void deleteMe() {
        deleted = true;
    }
    
    @Override
    public int hashCode() {
        return getId();
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Proxy) {
            Proxy proxy = (Proxy)obj;
            if(getId() == proxy.getId()) {
                return true;
            }
        }
        return false;
    }    
    
    /**
     * returns deleted flag switch
     * @return 
     */
    public boolean isDeleted() {
        return deleted;
    }
    
    /**
     * increments internal version count for proxy
     */
    public void incrementVersionCnt() {
        versionCnt++;
    }

    /**
     * @return the versionCnt
     */
    public long getVersionCnt() {
        return versionCnt;
    }

    /**
     * @param versionCnt the versionCnt to set
     */
    public void setVersionCnt(long versionCnt) {
        this.versionCnt = versionCnt;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the lastUpdated
     */
    public long getLastUpdated() {
        return lastUpdated;
    }

    /**
     * @param lastUpdated the lastUpdated to set
     */
    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
    
    public void debug() {
        System.out.println(persistObjs);
    }
    
    public String toString() {
        return "Proxy";
    }
    
}
