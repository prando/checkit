package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.persistence.framework.AccountProxy;

/**
 *
 * @author Paul
 * 
 * This class handles account insert persistence transaction
 */
public class InsertAccountTransaction extends InsertTransaction {

    public InsertAccountTransaction(AccountAbs account) throws PersistenceException {
        super(account);
    }
    
    @Override
    protected int insert() throws PersistenceException {
        AccountProxy accountProxy = (AccountProxy)proxy;
        
        int id = db.insert(accountProxy);
        return id;
    }
    
}
