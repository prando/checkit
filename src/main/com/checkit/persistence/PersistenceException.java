package com.checkit.persistence;

/**
 *
 * @author Paul
 * Base exception class for persistence level exceptions
 */
public class PersistenceException extends Exception {
    
    public PersistenceException(String msg) {
        super("Persistence Exception Occured: " + msg);
    }
}
