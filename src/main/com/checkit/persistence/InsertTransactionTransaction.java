package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;

/**
 *
 * @author Paul
 * 
 * This class handles Transaction insert into database
 */
public class InsertTransactionTransaction extends InsertTransaction {

    public InsertTransactionTransaction(Transaction transaction, AccountAbs account) throws PersistenceException {
        super(transaction, account);
    }
    
    @Override
    protected int insert() throws PersistenceException {
        Transaction transaction = (Transaction)businessObj;
        TransactionProxy transactionProxy = (TransactionProxy)proxy;
        transactionProxy.setAccountId(proxy2.getId());
        int id = db.insert(transactionProxy);
        return id;
    }
    
}
