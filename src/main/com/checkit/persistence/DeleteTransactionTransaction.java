package com.checkit.persistence;

import com.checkit.business.logic.AccountAbs;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;

/**
 *
 * @author Paul
 * 
 * This class handles delete Transaction transaction.
 */
public class DeleteTransactionTransaction extends DeleteTransaction {

    public DeleteTransactionTransaction(Transaction transaction, AccountAbs account) throws PersistenceException {
        super(transaction, account);
    }
    
    @Override
    protected void delete() throws PersistenceException {
        Transaction transaction = (Transaction)businessObj;
        TransactionProxy transactionProxy = (TransactionProxy)proxy;
        transactionProxy.setAccountId(proxy2.getId());
        db.delete(transactionProxy);

    }
    
}
