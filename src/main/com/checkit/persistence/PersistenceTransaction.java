package com.checkit.persistence;

import com.checkit.persistence.framework.CheckItPersistence;


/**
 *
 * @author Paul
 *
 * This abstract class is the base class which handles committal, persistence
 * transactional type activity to the database. This includes,
 * Insert/update/delete
 *
 */
public abstract class PersistenceTransaction {

    protected Database db = null;
    protected CheckItPersistence businessObj = null;
    protected CheckItPersistence businessObj2 = null;
    protected Proxy proxy = null;
    protected Proxy proxy2 = null;

    protected abstract void process() throws PersistenceException;

    public PersistenceTransaction(CheckItPersistence obj) throws PersistenceException {
        this(obj, null);
    }

    public PersistenceTransaction(CheckItPersistence obj, CheckItPersistence obj2) throws PersistenceException {
        this.businessObj = obj;
        this.businessObj2 = obj2;
    }
    
    protected void processPersistence() throws PersistenceException {

        this.proxy = businessObj.getPersistenceProxy();
        this.proxy.incrementVersionCnt();
        this.proxy.setLastUpdated(System.currentTimeMillis());

        if (businessObj2 != null) {
            this.proxy2 = businessObj2.getPersistenceProxy();
            this.proxy2.incrementVersionCnt();
            this.proxy2.setLastUpdated(System.currentTimeMillis());
        }

        db = Database.getInstance();

        //businessObj.debug();
        process();

        try {
            this.businessObj.mergePersistenceProxy(proxy);
            //this.businessObj.updateFromProxy();

            if (businessObj2 != null) {
                this.businessObj2.mergePersistenceProxy(proxy2);
                //this.businessObj2.updateFromProxy();
            }
        } catch (ProxyAttributeNameException e) {
            e.printStackTrace();
        }


        db.commit();
    
    }
}
