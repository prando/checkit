package com.checkit.persistence;

import java.io.Serializable;

/**
 *
 * @author Paul
 */
public interface PersistenceObjectIfc extends Serializable {
    
    public int checkSum();
    
}
