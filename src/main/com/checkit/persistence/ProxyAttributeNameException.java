package com.checkit.persistence;

/**
 *
 * @author prando
 * Exception thrown when proxy attribute name exception exists. 
 *
 */
public class ProxyAttributeNameException extends Exception {
    
    public ProxyAttributeNameException(String msg) {
        super("Proxy Attribute Name Exception: " + msg);
    }
}
