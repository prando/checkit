package com.checkit.persistence;

import com.checkit.persistence.framework.CheckItPersistence;


/**
 *
 * @author Paul
 * This abstract class is a persistence transaction class which must be extended to handle updates to persistence layer.
 * This class accepts a persistence business object.  During processing, the proxy class will be extracted and processed against the database.
 * 
 */
public abstract class UpdateTransaction extends PersistenceTransaction {
    
    protected abstract void update() throws PersistenceException;
    
    public UpdateTransaction(CheckItPersistence obj) throws PersistenceException {
        super(obj, null);
        processPersistence();
    }
    
    public UpdateTransaction(CheckItPersistence obj, CheckItPersistence obj2) throws PersistenceException {
        super(obj, obj2);
        processPersistence();
    }
    
    protected void process() throws PersistenceException {

        update();
    }
}
