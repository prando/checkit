/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.utils;

import com.checkit.business.logic.Checking;
import com.checkit.persistence.Database;
import com.checkit.persistence.InsertAccountTransaction;
import com.checkit.persistence.PersistenceException;
import com.checkit.persistence.framework.Accounts;
import java.io.File;
import java.util.Calendar;

/**
 *
 * @author Paul
 */
public class TestBed {
    
    private static File testDbFile = null;
    private static Database instance = null;
    
    static {
        instance.getInstance();
    }
    
    public static void createTestBed() {
        testDbFile = new File("checkItTest.db");
        instance = Database.getInstance();
        
        try {
            instance.loadDatabase(testDbFile);
            instance.coldstart();
        }
        catch(PersistenceException e) {
            e.printStackTrace();
        }
    }
    
    public static void destroyTestBed() {
        try {
            instance.deleteAll();
            instance.closeDatabase();
        }
        catch(PersistenceException e) {
            e.printStackTrace();
        }
    }
    
    public static Checking insertAndReturnNewCheckingAccount(String accountName, int acctNumber) throws PersistenceException {
        Checking checking = new Checking(accountName, acctNumber, Calendar.getInstance());
        Accounts.insertAccount(checking);
        return checking;
    }
    
}
