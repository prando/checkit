package com.checkit.utils;

import java.io.Serializable;

/**
 *
 * @author Paul
 * 
 * Test Object used to test serialization to file.
 */
public class TestSerializationObject implements Serializable {

    public int id = -1;
    public String name = "";
    public long timestamp = -1L;
}
