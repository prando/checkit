package com.checkit.utils;

import com.checkit.persistence.Proxy;

/**
 *
 * @author Paul
 * Test Proxy class used during JUnit Tests
 * 
 */
public class TestProxy extends Proxy {

    private String name = null;
    private int number = -1;

    @Override
    public int checkSum() {
        return getId();
    }

    @Override
    public int compareTo(Object o) {
        return -1;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the number
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Proxy) {
            Proxy proxy = (Proxy) obj;
            if (getId() == proxy.getId()) {
                return true;
            }
        }
        return false;
    }
}
