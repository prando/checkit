package com.checkit.business.logic;

import com.checkit.persistence.Proxy;
import com.checkit.persistence.ProxyAttributeNameException;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.CheckingAccountProxy;
import java.util.Calendar;

/**
 * Checking account. Returns just what's in the account.
 */

public class Checking extends AccountAbs {
    /**
     * Initialize instance variables
     */
    
    //private int id;
    //private int accountNumber;
    //private double accountBalance;
    //private Calendar created;
    
    /**
     * Constructor
     */
    public Checking(String accountName, int accountNumber, Calendar created){
        super(accountName, accountNumber, created);
        //this.accountNumber = accountNumber;
        //this.created = created;
    }
    
    public Checking(CheckingAccountProxy proxy) {
        super(proxy);
    }
    
    
    /**
     * Method to get balance of account
     * @return account balance
     */
    @Override
    public double getBalance(){
        return accountBalance;
    }

    
    /**
     * Merge Persistence data from Proxy into class
     * @param proxy
     * @throws ProxyAttributeNameException 
     */
    @Override
    public void mergePersistenceProxy(Proxy proxy) throws ProxyAttributeNameException {
        CheckingAccountProxy accountProxy = (CheckingAccountProxy)proxy;
        id = accountProxy.getId();
        accountName = accountProxy.getString("accountName");
        accountNumber = accountProxy.getInteger("accountNumber");
        accountBalance = accountProxy.getDouble("accountBalance");
        created = accountProxy.getCalendar("created");
        
        //Add any additional fields below for new Business Logic
    }

    /**
     * Set Proxy information to be persisted into database from class
     * @return 
     */
    @Override
    public Proxy getPersistenceProxy() {
        CheckingAccountProxy proxy = new CheckingAccountProxy();
        proxy.setId(id);        
        proxy.setObject("accountName", getAccountName());
        proxy.setObject("accountNumber", getAccountNumber());
        proxy.setObject("accountBalance", getAccountBalance());
        proxy.setObject("created", getCreated());
        
        //Add any additional fields below for new Business Logic
        
        
        return proxy;
    }


}
