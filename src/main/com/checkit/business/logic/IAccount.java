package com.checkit.business.logic;
import java.util.Calendar;

/**
 * Interface for checking or saving account.
 */
public interface IAccount {
    /**
     * Get the account Name
     * 
     * @return the name of the account
     */
    public String getAccountName();
    
    /**
     * Get the total balance of the account.
     * 
     * @return the dollar amount of the account
     */
    public double getBalance();
  
    /**
     * Get the account number of the account.
     * 
     * @return account number
     */
    public int getAccountNumber();
    
    /**
     * Withdraw from the account.
     * @param amount the amount to withdraw
     */
    public void withdraw(double amount);
    
    /**
     * Deposit to the account.
     * @param amount the amount to deposit
     */ 
    public void deposit(double amount);
    
    /**
     * Get the date the account was created.
     */
    public Calendar getDate();
}
