package com.checkit.business.logic;

import com.checkit.persistence.Proxy;
import com.checkit.persistence.ProxyAttributeNameException;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.SavingsAccountProxy;
import java.util.Calendar;

/**
 * Saving account. Returns what's in the account plus interest.
 */

public class Savings extends AccountAbs {
    /**
     * Initialize instance variables
     */
    //private int id;
    //private int accountNumber;
    //private double accountBalance;
    //private Calendar created;
    
    /**
     * Constructor
     */
    public Savings(String accountName, int accountNumber, Calendar created, double interestRate){
        super(accountName, accountNumber, created);
        //this.accountNumber = accountNumber;
        //this.created = created;
    }
    
    public Savings(SavingsAccountProxy proxy) {
        super(proxy);
    }
    
    /**
     * Method to get balance of account
     * @return account balance
     */
    @Override
    public double getBalance(){
        /**
         * blah the date/calendar class hella sucks
         * maybe im just gonna use joda like everyone else ever
         */
        return accountBalance;
    }

    /**
     * Merge Persistence data from Proxy into class
     * @param proxy
     * @throws ProxyAttributeNameException 
     */
    @Override
    public void mergePersistenceProxy(Proxy proxy) throws ProxyAttributeNameException {
        SavingsAccountProxy accountProxy = (SavingsAccountProxy)proxy;
        id = accountProxy.getId();
        accountName = accountProxy.getString("accountName");
        accountNumber = accountProxy.getInteger("accountNumber");
        accountBalance = accountProxy.getDouble("accountBalance");
        created = accountProxy.getCalendar("created");
        
        //Add any additional fields below for new Business Logic
    }

    /**
     * Set Proxy information to be persisted into database from class
     * @return 
     */
    @Override
    public Proxy getPersistenceProxy() {
        SavingsAccountProxy proxy = new SavingsAccountProxy();
        proxy.setId(id);
        proxy.setObject("accountName", accountName);
        proxy.setObject("accountNumber", accountNumber);
        proxy.setObject("accountBalance", accountBalance);
        proxy.setObject("created", created);
        
        //Add any additional fields below for new Business Logic
        
        return proxy;
    }

    /**
     * Get Unique identifier of Account after it has been persisted to database
     * @return 
     */
    @Override
    public int getId() {
        return id;
    }
}
