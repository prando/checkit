package com.checkit.business.logic;
import com.checkit.persistence.framework.CheckItPersistence;
import com.checkit.persistence.Database;
import com.checkit.persistence.PersistenceException;
import com.checkit.persistence.Proxy;
import com.checkit.persistence.ProxyAttributeNameException;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.Accounts;
import com.checkit.persistence.framework.TransactionProxy;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

/**
 * Abstract superclass for bank accounts.
 */
public abstract class AccountAbs implements IAccount, CheckItPersistence {
    /**
     * Initialize instance variables
     */
    protected int id;
    protected String accountName;
    protected int accountNumber;
    protected double accountBalance;
    protected Calendar created;
    
    /**
     * Constructor
     */
    public AccountAbs(String accountName, int accountNumber, Calendar created){
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.created = created;
    }
    
    /**
     * Load Constructor and merging proxy.  This is used when loading Account information from database
     * @param proxy 
     */
    public AccountAbs(AccountProxy proxy) {
        try {
            mergePersistenceProxy(proxy);
        }
        catch(ProxyAttributeNameException e) {
            e.printStackTrace();
        }
        
    }
    
    /**
     * Method to add funds to an account.
     * Will not accept a negative value.
     * @param amount fund amount to add
     */
    @Override
    public void deposit(double amount){
        if(amount >= 0){
            setAccountBalance(getAccountBalance() + amount);
        }
        //Using console for now, once UI fully integrated probably use JFrame
        else{
            System.out.println("Invalid amount");
        }
    }
    
    /**
     * Method to withdraw funds from account.
     * Will not accept a negative value.
     * @param amount fund amount to remove
     */
    @Override
    public void withdraw(double amount){
        if(amount >= 0){
            setAccountBalance(getAccountBalance() - amount);
        }
        //Using console for now, once UI fully integrated probably use JFrame
        else{
            System.out.println("Invalid amount");
        }
    }    
    
    /**
     * Getter for accountName
     */
    @Override
    public String getAccountName(){
        return accountName;        
    }
    /**
     * Getter for the account number
     */
    @Override
    public int getAccountNumber(){
        return accountNumber;
    }
    /**
     * Getter for the date created.
     */
    @Override
    public Calendar getDate(){
        return getCreated();
    }
       
    //Getters for the account balance are in extended classes, as this is where
    //the behavior differs. Checking account returns balance as is; savings 
    //applies interest.
    
        /**
     * Overridden toString method
     */
    @Override
    public String toString(){
        String result = "Account number: "+accountNumber+" Creation date: "
                + getCreated();
        return result;
    }
    
    
    /**
     * Retrieves all Transactions for this Account (by Id) from the database
     * @return 
     */
    public ArrayList<Transaction> getAllTransactions() {
        return Accounts.getAllTransactionsByAccountId(getId());
    }
    
    /**
     * Persist a Transaction for this Account into the database
     * @param trx
     * @throws PersistenceException 
     */
    public void insertTransaction(Transaction trx) throws PersistenceException {
        Accounts.insertTransaction(trx, this);
    }
    
    /**
     * Deletes a Transaction for this account based on the ID of the persistence object
     * @param trx
     * @throws PersistenceException 
     */
    public void deleteTransaction(Transaction trx) throws PersistenceException {
        Accounts.deleteTransaction(trx, this);
    }

    /**
     * Get Unique identifier of Account after it has been persisted to database
     * @return 
     */
    public int getId() {
        return id;
    }

    /**
     * @return the accountBalance
     */
    public double getAccountBalance() {
        return accountBalance;
    }

    /**
     * @param accountBalance the accountBalance to set
     */
    public void setAccountBalance(double accountBalance) {
        this.accountBalance = accountBalance;
    }

    /**
     * @return the created
     */
    public Calendar getCreated() {
        return created;
    }

}
