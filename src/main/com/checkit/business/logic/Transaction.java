/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.business.logic;

import com.checkit.persistence.framework.CheckItPersistence;
import com.checkit.persistence.Proxy;
import com.checkit.persistence.ProxyAttributeNameException;
import com.checkit.persistence.framework.TransactionProxy;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Paul
 */
public class Transaction implements CheckItPersistence {

    private int accountId;
    private int id;
    private String trxType;   // 'd' for deposit 'w' for widrawal or purchase
    private String memo;
    private double amount;
    private Calendar date;
    
    
    public Transaction() {
    
    }
    
    
    
    
    /**
     * Merge Persistence data from Proxy into class
     * @param proxy
     * @throws ProxyAttributeNameException 
     */
    @Override
    public void mergePersistenceProxy(Proxy proxy) throws ProxyAttributeNameException {
        TransactionProxy trxProxy = (TransactionProxy) proxy;
        id = trxProxy.getId();
        accountId = trxProxy.getAccountId();
        setTrxType(trxProxy.getString("trxType"));
        setMemo(trxProxy.getString("memo"));
        setDate(trxProxy.getCalendar("date"));
        setAmount(trxProxy.getDouble("amount"));
        
        //Add any additional fields below for new Business Logic
    }
    
    

    /**
     * Set Proxy information to be persisted into database from class
     * @return 
     */
    @Override
    public Proxy getPersistenceProxy() {
        TransactionProxy trxProxy = new TransactionProxy();
        trxProxy.setId(id);
        trxProxy.setAccountId(accountId);
        trxProxy.setObject("trxType", getTrxType());
        trxProxy.setObject("memo", getMemo());
        trxProxy.setObject("date", getDate());
        trxProxy.setObject("amount", getAmount());
        
        //Add any additional fields below for new Business Logic

        return trxProxy;
    }

    @Override
    public int getId() {
        return id;
    }
    
    /**
     * 
     * @return the transaction type
     */
    public String getTrxType(){
        return trxType;
    }
    
    /**
     * @param the transaction type to set
     */
    public void setTrxType(String trxType){
        this.trxType = trxType;
    }

    /**
     * @return the memo
     */
    public String getMemo() {
        return memo;
    }

    /**
     * @param number the number to set
     */
    public void setMemo(String memo) {
        this.memo = memo;
    }

    /**
     * @return the amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }

    /**
     * @return the date
     */
    public Calendar getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Calendar date) {
        this.date = date;
    }
    
}
