/*
 * Class:      Jave Programming for Windows 90.301-001
 * Name:       Akram Husain
 * Due Date:    
 * Assignment: 
 * Desciption: 
 */
package com.checkit.ui;

/**
 *
 * @author Owner
 */
public class InvalidInputExceptions extends Exception{
    public InvalidInputExceptions(){}
    public InvalidInputExceptions(String msg){
        super(msg);
    }
}
