/*
 * Class:      Jave Programming for Windows 90.301-001
 * Name:       Akram Husain
 * Due Date:    
 * Assignment: 
 * Desciption: 
 */
package com.checkit.ui;

import com.checkit.business.logic.AccountAbs;
import com.checkit.persistence.Database;
import com.checkit.persistence.PersistenceException;
import com.checkit.persistence.framework.Accounts;
import java.text.SimpleDateFormat;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Owner
 */
public class AccountSummary extends javax.swing.JPanel {
    private Database db;
    private AccountAbs currentAccount;
    private String[] columnName = {"Created", "Name", "Number", "Balance"};
    private String[][] tableData;
    private DefaultTableModel tableModel = new DefaultTableModel();            
    private AddAccount addAccount;
    private JLabel accountName;
    private JLabel accountNumber;
    private JLabel accountBalance;
    /**
     * Creates new form AccountSummary
     */
    public AccountSummary() {
        initComponents();                    
    }
         
    public void getComps(Database db, AccountAbs currentAccount, AddAccount addAccount, 
            JLabel accountName, JLabel accountNumber, JLabel accountBalance  ){
        this.db = db;
        this.currentAccount = currentAccount;
        this.addAccount = addAccount;
        this.accountName = accountName;
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }
    
    protected void setModel(){        
        if(!db.isDbLoaded()){
             tableData = new String[][]{
                new String[]{"No Database", "Select a User"}};
        }
        else{                   
            if(Accounts.getAllAccounts().isEmpty()){
                tableData = new String[][]{
                    new String[]{"No Accounts", "Add Account"}};
            }
            else{
                tableData = new String[Accounts.getAllAccounts().size()][4];
                getData();   
            }
        }
            tableModel = new DefaultTableModel(tableData, columnName);
            tableAccountSummary.setModel(tableModel);        
    }
    
    private void getData(){
        int index = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        for(AccountAbs account : Accounts.getAllAccounts()){
            tableData[index][0] = sdf.format(account.getCreated().getTime());            
            tableData[index][1] = account.getAccountName();
            tableData[index][2] = Integer.toString(account.getAccountNumber());
            tableData[index][3] = "$ " + Double.toString(account.getAccountBalance());
            ++index;            
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPaneAS = new javax.swing.JScrollPane();
        tableAccountSummary = new javax.swing.JTable(){
            public boolean isCellEditable(int rowIndex, int cellIndex){
                return false;
            }
        };
        labelASTitle = new javax.swing.JLabel();
        buttonAdd = new javax.swing.JButton();
        buttonSelect = new javax.swing.JButton();
        buttonDelete = new javax.swing.JButton();

        tableAccountSummary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Number", "Balance"
            }
        ));
        tableAccountSummary.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tableAccountSummary.setFocusable(false);
        tableAccountSummary.setRequestFocusEnabled(false);
        scrollPaneAS.setViewportView(tableAccountSummary);

        labelASTitle.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        labelASTitle.setText("Summary Of Accounts");

        buttonAdd.setText("Add Account");
        buttonAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonAddActionPerformed(evt);
            }
        });

        buttonSelect.setText("Select Account");
        buttonSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSelectActionPerformed(evt);
            }
        });

        buttonDelete.setText("Delete Account");
        buttonDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPaneAS, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(159, 159, 159)
                .addComponent(labelASTitle)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(buttonAdd)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                .addComponent(buttonSelect)
                .addGap(53, 53, 53)
                .addComponent(buttonDelete)
                .addGap(44, 44, 44))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelASTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(scrollPaneAS, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonAdd)
                    .addComponent(buttonSelect)
                    .addComponent(buttonDelete))
                .addGap(11, 11, 11))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void buttonAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonAddActionPerformed
        if(!db.isDbLoaded()){
            new ErrorMessage("You must first select a user to add an account!").setVisible(true);
        }
        else{            
            addAccount.setVisible(true);
            this.setVisible(false);
        }
    }//GEN-LAST:event_buttonAddActionPerformed

    private void buttonDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonDeleteActionPerformed
        if(!db.isDbLoaded()){
            new ErrorMessage("You must first select a user!").setVisible(true);
        }
        else if(tableAccountSummary.getSelectedRow() == -1){
            new ErrorMessage("Please select an account").setVisible(true);
        }
        else{
        try {
            if(!Accounts.getAllAccounts().isEmpty()){
                if(currentAccount.getId() == Accounts.getAllAccounts().get(tableAccountSummary.getSelectedRow()).getId()){
                    currentAccount = null;
                    MainUI.setCurrentAccount(currentAccount);
                    accountName.setText("No Account Selected");
                    accountNumber.setText("00000000");
                    accountBalance.setText("$ 0.00");                            
                }                
                Accounts.deleteAccount(Accounts.getAllAccounts().get(tableAccountSummary.getSelectedRow()));
                
            }
            else{
                new ErrorMessage("You must add an account first before deleting.").setVisible(true);
            }
            setModel();
        } catch (PersistenceException ex) {
            new ErrorMessage(ex.getMessage()).setVisible(true);
        }
        }
    }//GEN-LAST:event_buttonDeleteActionPerformed

    private void buttonSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSelectActionPerformed
        if(!db.isDbLoaded()){
            new ErrorMessage("You must first select a user!").setVisible(true);
        }
        else if(tableAccountSummary.getSelectedRow() == -1){
            new ErrorMessage("Please select an account.").setVisible(true);
        }
        else{
            if(!Accounts.getAllAccounts().isEmpty()){
                currentAccount = Accounts.getAllAccounts().get(tableAccountSummary.getSelectedRow());
                MainUI.setCurrentAccount(currentAccount);
                accountName.setText(currentAccount.getAccountName());
                accountNumber.setText(Integer.toString(currentAccount.getAccountNumber()));
                accountBalance.setText("$ " + Double.toString(currentAccount.getAccountBalance()));            
            }
            else{
                new ErrorMessage("Please add an account to select.").setVisible(true);
            }
        }
    }//GEN-LAST:event_buttonSelectActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonAdd;
    private javax.swing.JButton buttonDelete;
    private javax.swing.JButton buttonSelect;
    private javax.swing.JLabel labelASTitle;
    private javax.swing.JScrollPane scrollPaneAS;
    private javax.swing.JTable tableAccountSummary;
    // End of variables declaration//GEN-END:variables
}
