/*
 * Class:      Jave Programming for Windows 90.301-001
 * Name:       Akram Husain
 * Due Date:    
 * Assignment: 
 * Desciption: 
 */
package com.checkit.ui;

/**
 *
 * @author Owner
 */
public class Validate{
    
    public Validate(){}
    
    public static String Name(String name) throws InvalidInputExceptions{
        for(char c : name.toCharArray()){
            if(!(c >= 48 && c <= 57) && 
               !(c >= 65 && c <= 90) &&
               !(c >= 97 && c <= 122)){               
                throw new InvalidInputExceptions("The name must be a letter\n" +
                                      "or a number (i.e. A-Z or 0-9");                
            }            
        }
        return name;
    }
    
    public static int Selection(int index)throws InvalidInputExceptions{
        if(index == 0){
            throw new InvalidInputExceptions("Please make sure to selct a choice\n" +
                                   "from all possible drop down selections");
        }
        return index;        
    }
    
    public static int Number(String number) throws InvalidInputExceptions{
        for(char c : number.toCharArray()){
            if(!(c >= 48 && c <= 57)){
                throw new InvalidInputExceptions("Account number only accepts numbers 0-9");
            }            
        }
        return Integer.parseInt(number);
    }

    
    public static int Year(String year) throws InvalidInputExceptions{
        try{
            int nYear = Integer.parseInt(year);        
            if(nYear <= 1900){
                throw new InvalidInputExceptions("The Year can not be earlier than 1900.");
            }
            return nYear;
        }catch(NumberFormatException ex){
            throw new InvalidInputExceptions("Please enter only number (0-9) " +
                                            "in the Year field.");
        }
    }
    
    public static int Month(String month) throws InvalidInputExceptions{
        try{                    
            int nMonth = Integer.parseInt(month);
            if(nMonth < 1 || nMonth > 12){
                throw new InvalidInputExceptions("The month must be a number" +
                                                 "representation between 1 - 12.");
             }
            return nMonth;
        }catch(NumberFormatException ex){
            throw new InvalidInputExceptions("Please enter only number (0-9) " +
                                            "in the Month field.");
        }
    }
    
    public static int Day(String day) throws InvalidInputExceptions{
        try{
            int nDay =  Integer.parseInt(day);
            if( nDay < 1 || nDay > 31){
                throw new InvalidInputExceptions("The day must be a number between 1 - 31.");
            }            
        return nDay;
        }catch(NumberFormatException ex){
            throw new InvalidInputExceptions("Please enter only number (0-9) " +
                                            "in the Day field.");
        }
    }
    
    public static double Amount(String amount) throws InvalidInputExceptions{
        try{
            double nAmount = Double.parseDouble(amount);        
            return nAmount;
        }catch(NumberFormatException ex){
            throw new InvalidInputExceptions("Please enter only number (0-9) " +
                                            "and a decimal point (.) for the Amount field.");
        }
    }
    
    public static String Memo(String memo) throws InvalidInputExceptions{
        if(memo.equals("Enter a brief description")){
            throw new InvalidInputExceptions("Please enter a Description.");
        }
        return memo;
    }
}
        

