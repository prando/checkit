/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import java.io.File;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class PersistenceUtilitiesTest {
    
    public PersistenceUtilitiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of getCheckSum method, of class PersistenceUtilities.
     */
    @Test
    public void testGetCheckSum() throws Exception {
        System.out.println("getCheckSum");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        long expResult = 0L;
        long result = instance.getCheckSum(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkReadPermissions method, of class PersistenceUtilities.
     */
    @Test
    public void testCheckReadPermissions() throws Exception {
        System.out.println("checkReadPermissions");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.checkReadPermissions(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkWritePermissions method, of class PersistenceUtilities.
     */
    @Test
    public void testCheckWritePermissions() throws Exception {
        System.out.println("checkWritePermissions");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.checkWritePermissions(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of checkSerialization method, of class PersistenceUtilities.
     */
    @Test
    public void testCheckSerialization() throws Exception {
        System.out.println("checkSerialization");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.checkSerialization(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of copyFile method, of class PersistenceUtilities.
     */
    @Test
    public void testCopyFile() throws Exception {
        System.out.println("copyFile");
        File src = null;
        File des = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        File expResult = null;
        File result = instance.copyFile(src, des);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeObjectToFile method, of class PersistenceUtilities.
     */
    @Test
    public void testWriteObjectToFile() throws Exception {
        System.out.println("writeObjectToFile");
        File file = null;
        Object obj = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        instance.writeObjectToFile(file, obj);
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeObjectsToFile method, of class PersistenceUtilities.
     */
    @Test
    public void testWriteObjectsToFile() throws Exception {
        System.out.println("writeObjectsToFile");
        File file = null;
        ArrayList<? extends Object> objs = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        instance.writeObjectsToFile(file, objs);
        fail("The test case is a prototype.");
    }

    /**
     * Test of readObjectsFromFile method, of class PersistenceUtilities.
     */
    @Test
    public void testReadObjectsFromFile() throws Exception {
        System.out.println("readObjectsFromFile");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        ArrayList expResult = null;
        ArrayList result = instance.readObjectsFromFile(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of createFile method, of class PersistenceUtilities.
     */
    @Test
    public void testCreateFile() throws Exception {
        System.out.println("createFile");
        String name = "";
        PersistenceUtilities instance = new PersistenceUtilities();
        File expResult = null;
        File result = instance.createFile(name);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of isFileNameExists method, of class PersistenceUtilities.
     */
    @Test
    public void testIsFileNameExists() {
        System.out.println("isFileNameExists");
        String name = "";
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.isFileNameExists(name);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of isFileExists method, of class PersistenceUtilities.
     */
    @Test
    public void testIsFileExists() {
        System.out.println("isFileExists");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.isFileExists(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFile method, of class PersistenceUtilities.
     */
    @Test
    public void testGetFile() {
        System.out.println("getFile");
        String name = "";
        PersistenceUtilities instance = new PersistenceUtilities();
        File expResult = null;
        File result = instance.getFile(name);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFileByName method, of class PersistenceUtilities.
     */
    @Test
    public void testDeleteFileByName() {
        System.out.println("deleteFileByName");
        String name = "";
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.deleteFileByName(name);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of deleteFile method, of class PersistenceUtilities.
     */
    @Test
    public void testDeleteFile() {
        System.out.println("deleteFile");
        File file = null;
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.deleteFile(file);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }

    /**
     * Test of renameFile method, of class PersistenceUtilities.
     */
    @Test
    public void testRenameFile() {
        System.out.println("renameFile");
        File file = null;
        String newName = "";
        PersistenceUtilities instance = new PersistenceUtilities();
        boolean expResult = false;
        boolean result = instance.renameFile(file, newName);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
}
