/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.util.Calendar;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class UpdateTransactionTransactionTest {
    
    public UpdateTransactionTransactionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of update method, of class UpdateTransactionTransaction.
     */
    @Test
    public void testUpdate() throws Exception {
        double expResult = 450.01;
        Checking checking = TestBed.insertAndReturnNewCheckingAccount("Name", 123);
        Transaction trx = new Transaction();
        trx.setAmount(500.50);
        trx.setDate(Calendar.getInstance());
        //trx.setNumber(1);
        
        Accounts.insertTransaction(trx, checking);
        
        Transaction insTrx = Accounts.getTransactionById(checking.getId(), trx.getId());
        insTrx.setAmount(expResult);
        System.out.println("trxId: " + insTrx.getId());
        Accounts.updateTransaction(insTrx, checking);
        
        Transaction updTrx = Accounts.getTransactionById(checking.getId(), trx.getId());
        System.out.println("updTrxId: " + updTrx.getId());
        if(expResult != updTrx.getAmount()) {
            fail("testUpdate() failed!");
        }

    }
}
