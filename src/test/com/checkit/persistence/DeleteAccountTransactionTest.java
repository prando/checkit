/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.util.Calendar;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class DeleteAccountTransactionTest {
    
    public DeleteAccountTransactionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of delete method, of class DeleteAccountTransaction.
     */
    @Test
    public void testDelete() throws Exception {
        Checking checking = new Checking("Name", 500, Calendar.getInstance());
        checking.setAccountBalance(100.00);
        Accounts.insertAccount(checking);

        int id = checking.getId();

        Checking acct = (Checking)Accounts.getAccountById(id);
        System.out.println("insAcct: " + acct);

        Accounts.deleteAccount(checking);

        Checking delAcct = (Checking)Accounts.getAccountById(id);
        System.out.println("delAcct: " + delAcct);
        
        if(delAcct != null) {
            fail("testDelete() failed!");
        }
    }
}
