package com.checkit.persistence;

import com.checkit.utils.TestProxy;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class ProxyTest {
    
    public ProxyTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of checkSum method, of class Proxy.
     */
    @Test
    public void testCheckSum() {
        Proxy instance = new TestProxy();
        int expResult = -1;
        int result = instance.checkSum();
        assertEquals(expResult, result);
    }

    /**
     * Test of getString method, of class Proxy.
     */
    @Test
    public void testGetString() throws Exception {
        String name = "testName";
        Proxy instance = new TestProxy();
        instance.setObject(name, "testValue");
        String expResult = "testValue";
        String result = instance.getString(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDouble method, of class Proxy.
     */
    @Test
    public void testGetDouble() throws Exception {
        String name = "testName";
        Proxy instance = new TestProxy();
        instance.setObject(name, 5.5);
        double expResult = 5.5;
        double result = instance.getDouble(name);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getFloat method, of class Proxy.
     */
    @Test
    public void testGetFloat() throws Exception {
        String name = "testName";
        Proxy instance = new TestProxy();
        instance.setObject(name, 4.4F);
        float expResult = 4.4F;
        float result = instance.getFloat(name);
        assertEquals(expResult, result, 0.0);
    }

    /**
     * Test of getInteger method, of class Proxy.
     */
    @Test
    public void testGetInteger() throws Exception {
        String name = "testName";
        Proxy instance = new TestProxy();
        instance.setObject(name, 500);
        int expResult = 500;
        int result = instance.getInteger(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of getDate method, of class Proxy.
     */
    @Test
    public void testGetDate() throws Exception {
        String name = "testName";
        Date expResult = new Date();
        Proxy instance = new TestProxy();
        instance.setObject(name, expResult);
        Date result = instance.getDate(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of getObject method, of class Proxy.
     */
    @Test
    public void testGetObject() throws Exception {
        String name = "testName";
        Proxy instance = new TestProxy();
        Object expResult = new String("test");
        instance.setObject(name, expResult);
        Object result = instance.getObject(name);
        assertEquals(expResult, result);
    }

    /**
     * Test of setObject method, of class Proxy.
     */
    @Test
    public void testSetObject() {
        String name = "testName";
        Object obj = new String("test");
        Proxy instance = new TestProxy();
        instance.setObject(name, obj);
    }

    /**
     * Test of deleteMe method, of class Proxy.
     */
    @Test
    public void testDeleteMe() {
        Proxy instance = new TestProxy();
        instance.deleteMe();
        if(!instance.isDeleted()) {
            fail("Test Failed");
        }
    }

    /**
     * Test of hashCode method, of class Proxy.
     */
    @Test
    public void testHashCode() {
        Proxy instance = new TestProxy();
        int expResult = 100;
        instance.setId(expResult);
        int result = instance.hashCode();
        assertEquals(expResult, result);
    }

    /**
     * Test of equals method, of class Proxy.
     */
    @Test
    public void testEquals() {
        Proxy instance = new TestProxy();
        instance.setId(100);
        Proxy instance2 = new TestProxy();
        instance2.setId(100);
        boolean expResult = true;
        boolean result = instance.equals(instance2);
        assertEquals(expResult, result);
    }

    /**
     * Test of isDeleted method, of class Proxy.
     */
    @Test
    public void testIsDeleted() {
        Proxy instance = new TestProxy();
        instance.deleteMe();
        boolean expResult = true;
        boolean result = instance.isDeleted();
        assertEquals(expResult, result);
    }

    /**
     * Test of incrementVersionCnt method, of class Proxy.
     */
    @Test
    public void testIncrementVersionCnt() {
        Proxy instance = new TestProxy();
        long expResult = 1L;
        instance.incrementVersionCnt();
        long result = instance.getVersionCnt();
        assertEquals(expResult, result);
    }

    /**
     * Test of getVersionCnt method, of class Proxy.
     */
    @Test
    public void testGetVersionCnt() {
        Proxy instance = new TestProxy();
        long expResult = 0L;
        long result = instance.getVersionCnt();
        assertEquals(expResult, result);
    }

    /**
     * Test of setVersionCnt method, of class Proxy.
     */
    @Test
    public void testSetVersionCnt() {
        long expResult = 20L;
        Proxy instance = new TestProxy();
        instance.setVersionCnt(expResult);
        long result = instance.getVersionCnt();
        assertEquals(expResult, result);
    }

    /**
     * Test of getId method, of class Proxy.
     */
    @Test
    public void testGetId() {
        Proxy instance = new TestProxy();
        int expResult = 10;
        instance.setId(expResult);
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of setId method, of class Proxy.
     */
    @Test
    public void testSetId() {
        Proxy instance = new TestProxy();
        int expResult = 10;
        instance.setId(expResult);
        int result = instance.getId();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLastUpdated method, of class Proxy.
     */
    @Test
    public void testGetLastUpdated() {
        Proxy instance = new TestProxy();
        long expResult = System.currentTimeMillis();
        instance.setLastUpdated(expResult);
        long result = instance.getLastUpdated();
        assertEquals(expResult, result);
    }

    /**
     * Test of setLastUpdated method, of class Proxy.
     */
    @Test
    public void testSetLastUpdated() {
        Proxy instance = new TestProxy();
        long expResult = System.currentTimeMillis();
        instance.setLastUpdated(expResult);
        long result = instance.getLastUpdated();
        assertEquals(expResult, result);
    }



}
