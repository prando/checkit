/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class SequencesTest {
    
    public SequencesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of reset method, of class Sequences.
     */
    @Test
    public void testReset() {
        Sequences instance = new Sequences();
        instance.reset();
        int result = instance.getPeekNextSequence();
        assertEquals(1, result);
    }

    /**
     * Test of getNextSequence method, of class Sequences.
     */
    @Test
    public void testGetNextSequence() {
        Sequences instance = new Sequences();
        int expResult = instance.getPeekNextSequence();
        int result = instance.getNextSequence();
        assertEquals(expResult, result);
    }

    /**
     * Test of getPeekNextSequence method, of class Sequences.
     */
    @Test
    public void testGetPeekNextSequence() {
        Sequences instance = new Sequences();
        instance.reset();
        int expResult = 1;
        int result = instance.getPeekNextSequence();
        assertEquals(expResult, result);
    }

    /**
     * Test of checkSum method, of class Sequences.
     */
    @Test
    public void testCheckSum() {
        Sequences instance = new Sequences();
        int expResult = instance.hashCode();
        int result = instance.checkSum();
        assertEquals(expResult, result);
    }
}
