/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.util.Calendar;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class DeleteTransactionTransactionTest {
    
    public DeleteTransactionTransactionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of delete method, of class DeleteTransactionTransaction.
     */
    @Test
    public void testDelete() throws Exception {
        Checking checking = TestBed.insertAndReturnNewCheckingAccount("Name", 123);
        Transaction trx = new Transaction();
        trx.setAmount(500.00);
        trx.setDate(Calendar.getInstance());
        //trx.setNumber(1);
        
        Accounts.insertTransaction(trx, checking);
        
        Transaction insTrx = Accounts.getTransactionById(checking.getId(), trx.getId());
        System.out.println("insTrx: " + insTrx);
        Accounts.deleteTransaction(trx, checking);
        
        Transaction delTrx = Accounts.getTransactionById(checking.getId(), trx.getId());
        System.out.println("delTrx: " + delTrx);
        if(delTrx != null) {
            fail("testDelete() failed!");
        }
    }
}
