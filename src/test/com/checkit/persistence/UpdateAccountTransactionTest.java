/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.io.File;
import java.util.Calendar;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class UpdateAccountTransactionTest {
    
    public UpdateAccountTransactionTest() {
        
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of update method, of class UpdateAccountTransaction.
     */
    @Test
    public void testUpdate() throws Exception {
        Checking checking = new Checking("Name",500, Calendar.getInstance());
        checking.setAccountBalance(0.00);
        Accounts.insertAccount(checking);

        int id = checking.getId();
        checking.setAccountBalance(500.00);

        Accounts.updateAccount(checking);
        
        Checking acct = (Checking)Accounts.getAccountById(id);
        
        if(acct.getAccountBalance() != 500.00) {
            fail("testUpdate() failed!");
        }

    }
}
