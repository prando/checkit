/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.io.File;
import java.util.Calendar;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class InsertTransactionTransactionTest {
    

    public InsertTransactionTransactionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of insert method, of class InsertTransactionTransaction.
     */
    @Test
    public void testInsert() throws Exception {
        
        //int expResult = 1;
        Checking checking = TestBed.insertAndReturnNewCheckingAccount("Name", 123);
        Transaction trx = new Transaction();
        trx.setAmount(500.00);
        trx.setDate(Calendar.getInstance());
        //trx.setNumber(expResult);
        
        
        Accounts.insertTransaction(trx, checking);
        int expResult = trx.getId();
        
        Transaction insTrx = Accounts.getTransactionById(checking.getId(), trx.getId());
        assertEquals(expResult, insTrx.getId());
    }
}
