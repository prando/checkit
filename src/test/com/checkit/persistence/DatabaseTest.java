/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.utils.TestProxy;
import com.checkit.persistence.framework.AccountProxy;
import com.checkit.persistence.framework.TransactionProxy;
import java.io.File;
import java.util.ArrayList;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class DatabaseTest {
    
    private static File testDbFile = null;
    private static Database instance = null;
    
    public DatabaseTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        testDbFile = new File("checkItTest.db");
        instance = Database.getInstance();
        
        try {
            instance.loadDatabase(testDbFile);
            instance.coldstart();
        }
        catch(PersistenceException e) {
            e.printStackTrace();
        }
        
    }
    
    @AfterClass
    public static void tearDownClass() {
        try {
            instance.deleteAll();
            instance.closeDatabase();
        }
        catch(PersistenceException e) {
            e.printStackTrace();
        }
    }


    /**
     * Test of getOrigDbFilename method, of class Database.
     */
    @Test
    public void testGetOrigDbFilename() {
        String expResult = "checkItTest.db";
        String result = instance.getOrigDbFilename();
        assertEquals(expResult, result);
    }


    /**
     * Test of insert method, of class Database.
     */
    @Test
    public void testInsert() throws Exception {
        Proxy proxy = new TestProxy();
        int expResult = instance.getPeekNextSequence();
        int result = instance.insert(proxy);
        
        instance.commit();
        
        assertEquals(expResult, result);
        
        instance.deleteAll();
    }

    /**
     * Test of delete method, of class Database.
     */
    @Test
    public void testDelete() throws Exception {
        Proxy proxy = new TestProxy();
        instance.insert(proxy);
        
        instance.commit();
        
        instance.delete(proxy);
        
        instance.commit();
        
        instance.deleteAll();
    }

    /**
     * Test of update method, of class Database.
     */
    @Test
    public void testUpdate() throws Exception {
        TestProxy proxy = new TestProxy();
        proxy.setName("test1");
        proxy.setNumber(1);
        int id = instance.insert(proxy);
        
        instance.commit();
        
        proxy.setName("test2");
        proxy.setNumber(2);
        
        instance.update(proxy);
        
        instance.commit();
        
        TestProxy uProxy = (TestProxy)instance.selectById(id);
        assertEquals("test2", proxy.getName());
        
        instance.deleteAll();
    }
    
    /**
     * Test of update method, of class Database.
     */
    @Test
    public void testSwitchDatabase() throws Exception {
        
        File switchedDbFile = new File("switchTest.db");
        instance.switchDatabase(switchedDbFile);
        instance.switchDatabase(testDbFile);
        
        assertEquals("checkItTest.db", instance.getOrigDbFilename());
        
    }    
    
    

    /**
     * Test of selectById method, of class Database.
     */
    @Test
    public void testSelectById() throws Exception {
        TestProxy expProxy = new TestProxy();
        expProxy.setName("test1");
        expProxy.setNumber(1);
        
        int id = instance.insert(expProxy);
        
        instance.commit();
        
        Proxy result = instance.selectById(id);
        assertEquals(expProxy, result);
        instance.deleteAll();
    }

    /**
     * Test of commit method, of class Database.
     */
    @Test
    public void testCommit() throws Exception {
        instance.commit();
        instance.deleteAll();
    }

    /**
     * Test of getAllActiveAccountProxies method, of class Database.
     */
    @Test
    public void testGetAllActiveAccountProxies() throws Exception {
        
        
        ArrayList<AccountProxy> expResult = new ArrayList<AccountProxy>();
        for(int i = 0; i < 10; i++) {
            AccountProxy newAccount = new AccountProxy();
            newAccount.setObject("name", "test" + i);
            newAccount.setObject("number", 5000 + i);
            instance.insert(newAccount);
            expResult.add(newAccount);
            System.out.println("Inserting AcctProxy: " + newAccount.getId());
        }
        
        instance.commit();
        
        ArrayList<AccountProxy> result = instance.getAllActiveAccountProxies();
        for(AccountProxy proxy : result) {
            System.out.println("Looking for AcctProxy: " + proxy.getId());
            if(!expResult.contains(proxy)) {
                fail("Account Proxies don't match");
            }
        }
        
        instance.deleteAll();
    }

    /**
     * Test of getAllActiveTransactionProxiesByAccountId method, of class Database.
     */
    @Test
    public void testGetAllActiveTransactionProxiesByAccountId() throws Exception {
        AccountProxy proxy = new AccountProxy();
        proxy.setObject("name", "Test Account");
        
        int id = instance.insert(proxy);
        
        instance.commit();
        
        ArrayList<TransactionProxy> trxProxies = new ArrayList<TransactionProxy>();
        for(int i = 0; i < 200; i++) {
            TransactionProxy trx = new TransactionProxy();
            trx.setAccountId(proxy.getId());
            trx.setObject("number", i);
            trx.setObject("amount", 100 + i);
            
            instance.insert(trx);
            trxProxies.add(trx);
            System.out.println("Inserting TrxProxy: " + trx.getId());
        }
        
        instance.commit();
        
        ArrayList<TransactionProxy> result = instance.getAllActiveTransactionProxiesByAccountId(id);
        for(TransactionProxy trx : result) {
            System.out.println("Looking for TrxProxy: " + trx.getId());
            if(!trxProxies.contains(trx)) {
                fail("Transaction Proxies don't match");
            }
        }
        
        instance.deleteAll();

    }

    /**
     * Test of isDbLoaded method, of class Database.
     */
    @Test
    public void testIsDbLoaded() {
        boolean expResult = true;
        boolean result = instance.isDbLoaded();
        assertEquals(expResult, result);
    }

    /**
     * Test of getInstance method, of class Database.
     */
    @Test
    public void testGetInstance() {
        Database expResult = instance.getInstance();
        Database result = Database.getInstance();
        assertEquals(expResult, result);
    }
    
    
    
}
