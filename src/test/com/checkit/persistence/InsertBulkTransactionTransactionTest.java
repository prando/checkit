/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.checkit.persistence;

import com.checkit.business.logic.Checking;
import com.checkit.business.logic.Transaction;
import com.checkit.persistence.framework.Accounts;
import com.checkit.utils.TestBed;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Paul
 */
public class InsertBulkTransactionTransactionTest {
    
    public InsertBulkTransactionTransactionTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        TestBed.createTestBed();
    }
    
    @AfterClass
    public static void tearDownClass() {
        TestBed.destroyTestBed();
    }

    /**
     * Test of insert method, of class InsertBulkTransactionTransaction.
     */
    @Test
    public void testInsert() throws Exception {
        ArrayList<Transaction> trxs = new ArrayList<Transaction>();
        for(int i = 0; i < 100; i++) {
            Transaction trx = new Transaction();
            trx.setAmount(100 + i);
            trx.setDate(Calendar.getInstance());
            //trx.setNumber(i);
            trxs.add(trx);
        }
        
        Checking checking = TestBed.insertAndReturnNewCheckingAccount("Name", 123);
        
        Accounts.insertBulkTransactions(trxs, checking);
        
    }
}
