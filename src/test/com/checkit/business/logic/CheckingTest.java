package com.checkit.business.logic;

import java.util.Calendar;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Tests public methods in Checking class
 */
public class CheckingTest {
    private static double accountBalance;
    private static Calendar today;
    private static Checking account;
    
    /**
     * Setup
     */
    
    @BeforeClass
    public static void setUpClass(){
        account = new Checking("Name",123465, today);
    }
    
    @Test
    public void testGetBalance(){
        accountBalance = 0;
        assertEquals(accountBalance, account.getBalance());
    }
    @Test
    public void testDeposit(){
        accountBalance = 10;
        account.deposit(10);
        assertEquals(accountBalance, account.getBalance());        
    }
    
    @Test
    public void testWithdraw(){
        accountBalance = 0;
        account.withdraw(10);
        assertEquals(accountBalance, account.getBalance());
    }
    
    @Test
    public void testGetAccountNumber(){
        int testNum = 123465;
        assertEquals(testNum, account.getAccountNumber());
    }
    
    @Test
    public void testGetDate(){
        today = Calendar.getInstance();
        assertEquals(today, account.getDate());
    }
}
